import createPersistedState from 'vuex-persistedstate'
import SecureLS from "secure-ls";
let ls = new SecureLS({ isCompression: false });


export default ({store}) => {
    createPersistedState({
        key: 'todo',
        paths: undefined,
        storage: {
            getItem: (key) => ls.get(key),
            setItem: (key, value) => ls.set(key, value),
            removeItem: (key) => ls.remove(key),
        }
    })(store)
}
