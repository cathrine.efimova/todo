export const state = () => {
    return {
        notes: [],
    }
}
export const mutations = {
    /*Записываем новую заметку*/
    setNewNote(state, note) {
        state.notes.push(note);
    },
    /*Редактируем заметку*/
    editNote(state, note) {
        for (let item in state.notes) {
            if (state.notes[item].id == note.id) {
                state.notes[item] = note;
            }
        }
    },
    /*Удаляем запись*/
    delNote(state, id) {
        let noteIndex = state.notes.findIndex(item => item.id == id);
        state.notes.splice(noteIndex, 1);
    },
    /*Удаляем задачу*/
    delTodo(state, data) {
        for (let note in state.notes) {
            if (state.notes[note].id == data.parentId) {
                let todoIndex = state.notes[note].todos.findIndex(item => item.id == Number(data.delId))
                if (todoIndex > 0) {
                    state.notes[note].todos.splice(todoIndex, 1);
                }
            }
        }
    }
}

export const actions = {}
