FROM node:current-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . /usr/src/app


RUN rm yarn.lock
RUN yarn
RUN yarn build


ENV HOST 0.0.0.0
EXPOSE 3000

# start command
CMD [ "yarn", "generate" ]
